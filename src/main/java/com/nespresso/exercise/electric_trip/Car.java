package com.nespresso.exercise.electric_trip;

import java.math.BigDecimal;


public class Car {
	int initBaterrySize;
	int batterySize; 
	int lowSpeedPerformance; 
	int highSpeedPerformance;
	
	
	private String location;

	

	public int getInitBaterrySize() {
		return initBaterrySize;
	}

	public void setInitBaterrySize(int initBaterrySize) {
		this.initBaterrySize = initBaterrySize;
	}

	public String getLocation() {
		return location;
	}

	public int getBatterySize() {
		return batterySize;
	}

	public void setBatterySize(int batterySize) {
		this.batterySize = batterySize;
	}

	public int getLowSpeedPerformance() {
		return lowSpeedPerformance;
	}

	public void setLowSpeedPerformance(int lowSpeedPerformance) {
		this.lowSpeedPerformance = lowSpeedPerformance;
	}

	public int getHighSpeedPerformance() {
		return highSpeedPerformance;
	}

	public void setHighSpeedPerformance(int highSpeedPerformance) {
		this.highSpeedPerformance = highSpeedPerformance;
	}


	public Car(int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance) {
		super();
		this.batterySize = batterySize;
		this.initBaterrySize=batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
	}

	public void sart(Trip trip,boolean withSprint) {
		Double sizeBattery = Double.valueOf(trip.getDistance()/ (withSprint ? this.highSpeedPerformance:this.lowSpeedPerformance));
		Double batteryCons = (this.batterySize >= sizeBattery  ) ? (this.batterySize - sizeBattery) : this.batterySize;
		this.batterySize =batteryCons.intValue();
		//modify trip location
		this.location = trip.getEndLocatiion();
		
		
	}

	public String getBaterySize(){
		Double sizeBattery = Double.valueOf(this.batterySize)/Double.valueOf(this.initBaterrySize);
		BigDecimal a = BigDecimal.valueOf(sizeBattery);
		BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN).multiply(BigDecimal.valueOf(100));
		Integer batterySize  = roundOff.intValue();
		return batterySize.toString()+"%";
	}
	public boolean haveEnergyToContinue(Trip nexTrip) {
		double sizeBattery = nexTrip.getDistance()/this.lowSpeedPerformance;
		boolean nextCar = nexTrip.getCar()!=null ? (this.initBaterrySize==nexTrip.getCar().initBaterrySize): true;
		return this.batterySize > sizeBattery && nextCar ;
	}

	public void charge(int hoursOfCharge,Trip trip) {
		this.batterySize+= trip.kWhcharged*hoursOfCharge;
	}
	
	
	
}
