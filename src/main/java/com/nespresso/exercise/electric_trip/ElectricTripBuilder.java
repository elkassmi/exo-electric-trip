package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElectricTripBuilder {

	public static Map<String,Trip> buildTrip(String tripDetail) {
		Map<String,Trip> lRetour = new HashMap<>();
		String[] trips = tripDetail.split(NespressoConstants.regex);

		int sizeTrip = trips.length;
        int j=0;
        int k = 0;
		while(j<sizeTrip){
			j+=3;
			String depart = trips[k];
			int distance = Integer.valueOf(trips[k + 1]);
			String destination = trips[k + 2];
			Trip trip = new Trip(depart, destination, distance);
			k+=2;
			lRetour.put(depart, trip);
		}
	

		return lRetour;
	}

	public static Car buildCar(int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		Car car = new Car(batterySize,lowSpeedPerformance,highSpeedPerformance);
		return car;
		
	}

}
