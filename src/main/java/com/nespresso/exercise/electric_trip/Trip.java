package com.nespresso.exercise.electric_trip;

public class Trip {
	private String satrtLocation;
	private String EndLocatiion;
	private int distance;
	int kWhcharged;

	private Car car;

	public Trip(String satrtLocation, String endLocatiion, int distance) {
		super();
		this.satrtLocation = satrtLocation;
		EndLocatiion = endLocatiion;
		this.distance = distance;
	}

	public String getSatrtLocation() {
		return satrtLocation;
	}

	public void setSatrtLocation(String satrtLocation) {
		this.satrtLocation = satrtLocation;
	}

	public String getEndLocatiion() {
		return EndLocatiion;
	}

	public void setEndLocatiion(String endLocatiion) {
		EndLocatiion = endLocatiion;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

}
