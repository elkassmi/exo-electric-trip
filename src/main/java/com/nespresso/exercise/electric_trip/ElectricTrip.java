package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Map;

public class ElectricTrip {

	// List<Trip> trips ;
	Map<String, Trip> trips;
	Map<Integer, Trip> participants = new HashMap<>();
	static int index = 0;

	public ElectricTrip(String tripDetail) {
		trips = ElectricTripBuilder.buildTrip(tripDetail);
	}

	public int startTripIn(String start, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		Trip trip = trips.get(start);
		Car car = ElectricTripBuilder.buildCar(batterySize,
				lowSpeedPerformance, highSpeedPerformance);
		trip.setCar(car);

		// TODO id car int au lieu de String
		// TODO key map paricipant
		int idParticipant = index;
		participants.put(idParticipant, trip);
		index++;
		return idParticipant;
	}

	public void go(int participantId) {
		// 1. get car whith key
		Trip trip = participants.get(participantId); // TODO key participant

		// 2. start car
		Car car = trip.getCar();
		for (String key : trips.keySet()) {
			if (car.haveEnergyToContinue(trips.get(key))) {
				car.sart(trip,false);
				trip = trips.get(trip.getEndLocatiion());
			}

		}

	}

	public String locationOf(Integer participantId) {
		Trip trip = participants.get(participantId);
		return trip.getCar().getLocation();
	}

	public String chargeOf(int participantId) {
		Trip trip = participants.get(participantId);
		return trip.getCar().getBaterySize();
	}

	public void sprint(int participantId) {
		// 1. get car whith key
		Trip trip = participants.get(participantId); // TODO key participant

		// 2. start car
		Car car = trip.getCar();
		for (String key : trips.keySet()) {
			if (car.haveEnergyToContinue(trips.get(key))) {
				car.sart(trip,true);
				trip = trips.get(trip.getEndLocatiion());
			}

		}

	}

	public void charge(int id, int hoursOfCharge) {
		Trip trip = participants.get(id);
		trip.getCar().charge(hoursOfCharge);
		
	}

}
